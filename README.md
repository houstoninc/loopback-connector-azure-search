loopback-connector-azure-search
====

LoopBack connector for Microsoft Azure Search.

How to install?
====

Install the connector package to your project:
```
npm install --save loopback-connector-azure-search
```

Set-up a new data source that uses the connector. In `datasources.json`, put the following:
```json
{
  "myDataSourceName": {
    "name": "myDataSourceName",
    "connector": "loopback-connector-azure-search",
    "host": "YOUR AZURE SEARCH HOST, TYPICALLY https://myAccount.search.windows.net:443/",
    "adminOrQueryKey": "YOUR ADMIN/QUERY KEY",

    "allowNonUrlSafeIDs": true false,
    "retry": {
        "max": 3,
        "waitInMilliseconds": 50
    }
  }
}
```

You can name your data source as you like, here we used `myDataSourceName` as an example. You can find values for `YOUR AZURE SEARCH HOST` and `YOUR ADMIN/QUERY KEY` in Azure portal. Please consult Azure documentation if needed. Rest of the properties are optional and they can be left undefined. However, we recommend setting a retry policy as shown above which automatically retries failed requests. Currently, only a constant wait retries are supported.

When configuring model properties, you may want to set specific settings for Azure Search. For example:
```json
{
  "azureSearch": {
    "indexName": "my-index",
    "analyzers": [
        "See: https://docs.microsoft.com/en-us/rest/api/searchservice/custom-analyzers-in-azure-search"
    ]
  },

  "myViewProperty": {
    "azureSearch": {
      "propertyName": "myDatabaseProperty",
      "searchable": true false,
      "filterable": true false,
      "sortable": true false,
      "facetable": true false,
      "retrievable": true false,
      "analyzer": "See: https://docs.microsoft.com/rest/api/searchservice/language-support"
    }
  }
}
```

Special Azure Search settings are set via `azureSearch` configuration object, but this is not required -- connector will use its default values if nothing else is specified in your model configuration. Also, if you do not specify values for `searchable`, `filterable`, `sortable`, `facetable`, `retrievable` and `analyzer` properties, connector will use Azure Search default values which may change in future. Please consult LoopBack documentation for more details on how to configure models and Azure documentation on indexing options.

There are special Azure Search properties that can be mapped via `propertyName` configuration property. These special fields are just mappings so they are not created to index and they can only be accessed in special contexts. The available mapping properties are described in a table below.

| Special `propertyName` value | Mapped value
|:-------------- |:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| @odata.count   | When using `limit` and `offset` filters, the query is recognized as a pagination query. You can receive the total number of matching items in an index with this property. |
| @search.score  | Azure Search gives a score for each result. You can access the score value via this property.                                                                              |

When you have specified your models, you must create Azure Search indices from models. You can do this by calling `autoupdate()` connector method. When you change model properties, you can update indices by calling `autoupdate()` again. However, Azure Search has some limitations on what kind of updates to indices can be applied. In those cases, you will receive an error and are forced to use `automigrate()` that removes old indices and then creates the new ones. But keep in mind that this will remove all indices and all the data that they may contain! Please consult LoopBack documentation for more details.

You are now ready to start using Azure Search in your LoopBack application!

Supported features
====

The following LoopBack's base connector methods are implemented:

* connect()

* disconnect()

* autoupdate()

* automigrate()

* create()

* updateOrCreate()

* replaceOrCreate()

* all()

* save()

* count()

Connector debug string is `loopback:connector:azure-search` that can be enabled through `DEBUG` environment variable.

Type mappings
===

The type mappings of LoopBack and Azure Search indices are described in a table below.

| LoopBack type | Azure Search type                  | Notes
|:------------- |:---------------------------------- |:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| string        | Edm.String                         |                                                                                                                                                                                                                                                                                      |
| array         | Collection(Edm.String)             |                                                                                                                                                                                                                                                                                      |
| boolean       | Edm.Boolean                        |                                                                                                                                                                                                                                                                                      |
| date          | Edm.DateTimeOffset                 |                                                                                                                                                                                                                                                                                      |
| GeoPoint      | Edm.GeographyPoint                 |                                                                                                                                                                                                                                                                                      |
| number        | Edm.Int64, Edm.Int32 or Edm.Double | By default, Edm.Double is used. You can change this by setting `azureSearch.integerProperty = 32` or `azureSearch.integerProperty = 64` to map to Emd.Int32 or Emd.Int64 respectively. Alternatively, you can set `azureSearch.integerProperty = true` which defaults to Edm.Int64.  |

Attempt to use an unsupported type will result an error.

Special notes about filtering
===

All LoopBack-style filtering features are implemented (see https://loopback.io/doc/en/lb3/Querying-data.html). In addition, the following extra filtering operators are implemented:

* `any` operator can be used to filter `array` properties. For example: `{ where: { property: { 'any x': { x: 'variable' } } } }`, where `x` is a variable name you get to choose that is bound to a local scope where you can apply other filtering rules. This example ensures that `property` array contains at least one item with a value `'variable'`.
* `all` operator can be used to filter `array` properties. Usage: `{ where: { property: { 'all x': { x: 'variable' } } } }`, where `x` is a variable name you get to choose that is bound to a local scope where you can apply other filtering rules. This example ensures that `property` array contains only items with a value `'variable'`.
* If you need to perform a full-text search from all fields, you can use a special `*` wildcard field to indicate this: `{ where: { '*': 'search keyword' } }`.
* You can select searchable fields with `searchFields` filter. It works as `fields` filter, but instead filtering results, it only affects properties where search is made.

Change log
===

* 0.0.1 -- Initial release.
* 0.1.0 -- Add support for retry policies. Lots of improvements and bug fixes.
* 0.2.0 -- Add support for unsafe keys via `allowNonUrlSafeIDs` setting property.
* 0.3.0 -- Add support for `fields` and `order` filters.
* 0.3.1 -- Support `analyzer` field property.
* 0.4.0 -- Support `analyzers`, `searchAnalyzer` and `indexAnalyzer` index properties. Add `searchFields` filter. Fix `fields` filter. Inject special Azure Search properties to view properties.
* 0.4.1 -- Fix incorrectly handled star property. Fix how full-text search are escaped.
* 0.5.0 -- Add support for document delete. Use full words in full-text search. Fix minor bugs that violated the specification.
* 0.5.1 -- Add option `defaultIndexAction` that sets the default indexing action if it is not provided with data.
* 0.5.2 -- Fix `null` value where filtering. Fix logical query where filters.
* 0.5.3 -- Retry all other but 400 (Bad Request) errors.
* 0.6.0 -- Fix like operators. Change how like operators are parsed: now they internally use `search.ismatch` inside a filter clause unless search property is a special wildcard property. In order to use full-text search from other properties, `filterable` must be enabled for a property.
* 0.6.1 -- Add support for deleting documents with Loopback standard methods (`destroyAll`, `destroyById`).
